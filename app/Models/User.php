<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Laravel\Passport\HasApiTokens;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasApiTokens;

    protected $table = 'AP_User';
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'created_on';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'company_id', 'username', 'password', 'first_name', 'last_name',
        'email', 'active', 'type', 'phone', 'isApproved'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function findForPassport($username) {
        return $this->where('username', $username)->first();
    }

    public function accessTokens() {
        return $this->hasMany('App\Models\OauthAccessToken', 'user_id');
    }
}
