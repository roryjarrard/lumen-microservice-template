<?php

namespace App\Http\Middleware;

use Closure;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use App\Models\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use App\Models\Company;

class TokenVerifyMiddleware
{

    public function __construct()
    {

    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $token = $request->bearerToken();

            $identity_provider_url = env('IDENTITY_PROVIDER_URL');

            $client = new Client([
                'verify' => false,
                'base_uri' => $identity_provider_url,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ]
            ]);

            try {
                $response = $client->get('/api/getUserInfo');

                $responseBody = json_decode($response->getBody());
                $userInfo = $responseBody->userInfo;
            } catch (ClientException $e) {
                $statusCode = $e->getCode() >= 1000 || !is_numeric($e->getCode()) ? 500 : $e->getCode(); // mysql errors most likely
                $response = json_decode($e->getResponse()->getBody());

                return [
                    'success' => false,
                    'title' => $response->title,
                    'message' => $response->message,
                    'origin_message' => $response->origin_message,
                    'origin_status' => $response->origin_status,
                    'statusCode' => $statusCode
                ];
            }

            if ($userInfo->user_id) {
                $user = User::find($userInfo->user_id);
            }
            $request->user = $user;

            if( $userInfo->company_id ) {
                $companyId = $userInfo->company_id;
                $company = Company::where('company_id', $companyId)->first();
            } else {
                $company = Company::find($user->company_id);
            }
            $db = $company->database_name;
            if(is_null($db)) {
                $db = env('DEFAULT_DB');
            }

            Config::set('database.connections.cardata.database',$db);
            DB::reconnect('cardata');
            $request->userTime = $request->has('userTime') ? $request->get('userTime') : date('Y-m-d H:i:s');
        } catch (\Exception $e) {
                return response()->json(['status' => 'Authorization Token not found', 'message' => $e->getMessage()], 400);
        }

        return $next($request);
    }
}
