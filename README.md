# <u>Microservice Template</u>
update 2012-12-20

The template has 3 different versions (by branch name):

* master
    * A simple Lumen project with CORS enabled
* passport
    * Based on the CORS master, Will employ Passport token generation, and is the primary authorization template (used in Identity Profile).
    * You probably will not use this template, and use the token validation version 
* token-validation
    * Will expect a token in requests, but will validate the token against a "passport" project (in this case, assumes the Identity Provider)
    
Both templates will employ CORS, and will allow for database switching based on company settings (may be disabled).

For any version, pull and `composer install`. Copy `.env.example` to `.env` and update your values accordingly (local database connections), app name, etc.

Instructions on how these templates were created follow.

1. ## Master Template
    ```
    > composer global require "laravel/lumen-installer
    > lumen new microservice-template
    > cd microservice-template
    > composer require ext-json
    > git init
    > git add .
    > git commit -m "initial commit"
    > cp .env.example .env
    > date | md5sum
    ba906e08988116cd42e20aa165cc95a5 *-
    ```
    
    The `date | md5sum` is an easy way to generate a random string. This was copied into
    the .env file. Database values would have to include your local credentials, timezone if desired.
    
    ## Adding CORS Middleware
    
    ```
    > touch app/Http/Middleware/CorsMiddleware.php
    ```
    Contents of CorsMiddleware.php
    ```
    <?php
    
    namespace App\Http\Middleware;
    
    use Closure;
    
    class CorsMiddleware
    {
        /**
        * Handle an incoming request.
        *
        * @param  \Illuminate\Http\Request $request
        * @param  \Closure $next
        * @return mixed
        */
        public function handle($request, Closure $next)
        {
            $headers = [
                'Access-Control-Allow-Origin' => '*',
                'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE, PATCH',
                'Access-Control-Allow-Credentials' => 'true',
                'Access-Control-Max-Age' => '86400',
                'Access-Control-Allow-Headers' => 'Content-Type, Authorization, X-Requested-With'
            ];
    
            if ($request->isMethod('OPTIONS')) {
                return response()->json('{"method":"OPTIONS"}', 200, $headers);
            }
    
            $response = $next($request);
            foreach ($headers as $key => $value) {
                $response->header($key, $value);
                // if error then use $response->headers->set($key, $value);
            }
    
            return $response;
        }
    }
    ```
    
    ***Notice the comment about a potential header error!***
    
    Then add to bootstrap/app.php
    ```
    $app->withFacades();  <-- uncomment
    
    $app->withEloquent(); <-- uncomment
    
    $app->configure('database');  <-- add
    ...
    $app->middleware([
        App\Http\Middleware\CorsMiddleware::class
    ]);
    ```
    
    * Add database configuration
    
        ```
        > mkdir config
        > touch config/database.php
        ```
        
        Contents of `config/database.php`
        
    
     ## Dockerizing the project
     
     ```
     > mkdir -p .docker/certs
     > touch .docker/custom.ini
     > touch .docker/default.conf
     > touch .docker/nginx.conf
     > touch .docker/Dockerfile
     > touch docker-compose.yml
     ```
     
    ### Contents of nginx.conf
    ``` 
    user  nginx;
    worker_processes  1;
    
    error_log  /var/log/nginx/error.log warn;
    pid        /var/run/nginx.pid;
    
    events {
        worker_connections  1024;
    }
    
    http {
        include       /etc/nginx/mime.types;
        default_type  application/octet-stream;
    
        log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                          '$status $body_bytes_sent "$http_referer" '
                          '"$http_user_agent" "$http_x_forwarded_for"';
    
        access_log  /var/log/nginx/access.log  main;
    
        sendfile        on;
        #tcp_nopush     on;
    
        keepalive_timeout  65;
    
        gzip  on;
        gzip_disable "msie6";
        gzip_min_length 256;
        gzip_types
            text/plain
            text/css
            application/json
            application/x-javascript
            application/javascript
            text/xml
            application/xml
            application/xml+rss
            text/javascript
            application/vnd.ms-fontobject
            application/x-font-ttf
            font/opentype
            image/svg+xml
            image/x-icon
        ;
    
        include /etc/nginx/conf.d/*.conf;
    }
    ```
    
    ### Contents of default.conf
    ```
    server {
        listen 443 ssl;
        server_name microservice-template.cardata.loc;
        server_tokens off;
        index index.php;
        root /var/www/html/public;
    
        ssl_certificate /etc/nginx/certs/microservice-template.crt;
        ssl_certificate_key /etc/nginx/certs/microservice-template.key;
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_ciphers 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:DHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:DHE-RSA-CAMELLIA256-SHA:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-SEED-SHA:DHE-RSA-CAMELLIA128-SHA:HIGH:!aNULL:!eNULL:!LOW:!3DES:!MD5:!EXP:!PSK:!SRP:!DSS';
        ssl_prefer_server_ciphers on;
        ssl_session_timeout 5m;
        ssl_session_cache shared:SSL:5m;
        add_header Strict-Transport-Security "max-age=31536000; includeSubDomains always";
    
        location / {
           try_files $uri $uri/ /index.php?$query_string;
        }
    
        location ~ \.php {
            try_files $uri =404;
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            fastcgi_pass unix:/var/run/php/php7.3-fpm.sock;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include fastcgi_params;
        }
    }
    ```
    Of course you will change the server name and the naming of the certs, but they should end in .cardata.loc for local development
    
    ### Contents of custom.ini
    ```
    date.timezone = ${PHP_DATE_TIMEZONE}
    memory_limit = 2G
    max_execution_time = 30
    
    [xdebug]
    xdebug.default_enable=${PHP_XDEBUG_DEFAULT_ENABLE}
    xdebug.remote_autostart=${PHP_XDEBUG_REMOTE_AUTOSTART}
    xdebug.remote_connect_back=${PHP_XDEBUG_REMOTE_CONNECT_BACK}
    xdebug.remote_host=${PHP_XDEBUG_REMOTE_HOST}
    xdebug.remote_port=${PHP_XDEBUG_REMOTE_PORT}
    xdebug.remote_enable=${PHP_XDEBUG_REMOTE_ENABLE}
    xdebug.remote_log=${PHP_XDEBUG_REMOTE_LOG}
    xdebug.idekey=${PHP_XDEBUG_IDEKEY}
    xdebug.profiler_enable=${PHP_XDEBUG_PROFILER_ENABLE}
    xdebug.profiler_output_dir=${PHP_XDEBUG_PROFILER_OUTPUT_DIR}
    ```
    
    ### Contents of Dockerfile
    ```
    FROM wyveo/nginx-php-fpm:php73
    
    LABEL maintainer="CarData Development Team"
    
    RUN rm /etc/nginx/conf.d/default.conf
    
    COPY .docker/nginx/nginx.conf /etc/nginx/nginx.conf
    COPY .docker/nginx/api.conf /etc/nginx/conf.d/
    COPY .docker/nginx/certs/* /etc/nginx/certs/
    
    
    # Needed or else our apt-get will fail
    RUN wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
    
    RUN apt-get -yqq update \
        && apt-get -yqq install --no-install-recommends apt-utils libzip-dev gnupg libpng-dev vim curl less \
        && apt-get install -y --allow-unauthenticated php-pear \
        && apt-get install -yqq --allow-unauthenticated php7.3-dev \
        && apt-get install -yqq --allow-unauthenticated make \
        && pecl install xdebug \
        && echo "zend_extension=$(find /usr/lib/php/20180731/ -name xdebug.so)" > /etc/php/7.3/fpm/conf.d/xdebug.ini
    
    COPY .docker/nginx/custom.ini /etc/php/7.3/fpm/conf.d/
    
    RUN service nginx restart && service php7.3-fpm restart
    ```
    
    ### Contents of docker-compose.yml
    ```
    version: '3'
    
    networks:
      web:
        external: true
      internal:
        external: true
    
    services:
      nginx:
        build:
          context: .
          dockerfile: .docker/nginx/Dockerfile
        container_name: microservice-template <-- change this
        env_file:
          - .env
        networks:
          - web
          - internal
        expose:
          - 443
        volumes:
          - .:/var/www/html
        environment:
          - VIRTUAL_HOST=microservice-template.cardata.loc <-- change this
          - VIRTUAL_PROTO=https
          - VIRTUAL_PORT=443
    ```
    
    ### Final Steps
    1. Add the project name (project-name.cardata.loc) to your hosts file
    2. Go to https://www.selfsignedcertificate.com/ and type in the name of the project (*.cardata.loc). This will
        generate a key and a certificate. 
    3. Copy the key and cert (*.cert) to .docker/certs/, changing the name to project-name.cardata.loc.key and project-name.cardata.loc.crt.
        Make sure they match the names in the default.conf file.
    4. Copy .docker/certs/* to `your_path/api-reverse-proxy/certs/`
    5. Rebuild api-reverse-proxy
    6. Rebuild this project
    
    The final step wil be to move the models to a dedicated folder. We don't care in the master branch, but this will be used in
    the other branches.
        
    ```
    > mkdir -p app/Models
    > mv app/User.php app/Models/User.php
    ```
    Then update the namespace in User.php to "App\Models".
    
2. ## Passport Template
    
    * Install lumen passport package
        ```
        > git co -b passport
        > git add .
        > git commit -m "branch for passport"
        > git push -u origin passport
        > composer require dusterio/lumen-passport
        > mkdir -p config
        > touch config/auth.php
        > touch config/database.php
        ```
        
        Contents of `config/auth.php`
        ```
        <?php
            
        return [
            'defaults' => [
                'guard' => 'api',
                'passwords' => 'users',
            ],
        
            'guards' => [
                'api' => [
                    'driver' => 'passport',
                    'provider' => 'users',
                ],
            ],
        
            'providers' => [
                'users' => [
                    'driver' => 'eloquent',
                    'model' => \App\Models\User::class
                ]
            ]
        ];
        ```
        
        Contents of `config/database.php`
        ```
        
        ```
          
    * Add the following to `bootstrap/app.php`
        ```
        // enable use of facades
        $app->withFacades();
    
        // enable Eloquent
        $app->withEloquent();
    
        // load auth config
        $app->configure('auth');
        $app->configure('database');
        ...
        $app->register(Laravel\Passport\PassportServiceProvider::class);
        $app->register(Dusterio\LumenPassport\PassportServiceProvider::class);
        ...
        $app->routeMiddleware([
            'auth' => App\Http\Middleware\Authenticate::class,
            'client' => \Laravel\Passport\Http\Middleware\CheckClientCredentials::class,
        ]);
        ```
        
    * Create route group for Passport in `routes/web.php`
        ```
        $router->group(['middleware' => ['client'], 'prefix' => 'api'], function () use ($router) {
            
        });
        ```
    
    * Run migrations
        
        `> php artisan migrate`
        
        This creates the following schema
        * oauth_access_tokens
        * oauth_auth_codes
        * oauth_clients
        * oauth_personal_access_clients
        * oauth_refresh_tokens
    
    * Install Laravel Passport
    
        `> php artisan passport:install`
        
        This will generate private and public encryption keys used for token generation and sets up necessary routes. Keys are stored in the **storage** folder
        * oauth-private.key
        * oauth-public.key
        
        You will also see two new records in the oauth_clients table, a ***personal_access_client*** and a ***password_client***
        
    * Set-up route prefix
    
        This statement is added to your **bootstrap/app.php** file after registering the service providers.
        
        `Dusterio\LumenPassport\LumenPassport::routes($app->router, ['prefix' => 'api/pp/oauth']);`
    
    * Add HasAPITokens trait to your User model, and update to fit our table. Also, have Passport use the username for identification (the default is email)
    
            ```
            ...
            use Laravel\Passport\HasApiTokens;
            
            class User extends Model implements AuthenticatableContract, AuthorizableContract
            {
                use Authenticatable, Authorizable, HasApiTokens;
                                                   ------------
                
                protected $fillable = [
                    'username', 'first_name', 'last_name', 'email'
                ];
                
                ...
                
                public function findForPassport($username) {
                    return $this->where('username', $username)->first();
                }
            ```
            
    
        
        
     
    
    
        
     
     
    
     


    
